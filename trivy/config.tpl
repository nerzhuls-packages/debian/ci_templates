{{- range . -}}
{{ if ne (len .Misconfigurations) 0 }}
# Trivy config scan results
| Type | Title | Severity | Description | Message | Resolution |
| --- | --- | --- | --- | --- | --- |
{{- range .Misconfigurations }}
| {{ .Type }} | {{ .Title }} | {{ .Severity }} | {{ .Description }} | {{ .Message }} | {{ .Resolution }} |
{{- end }}
{{- end }}
{{- end }}
